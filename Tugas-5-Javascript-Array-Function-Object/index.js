//soal 1
var daftarHewan = ["2. Komodo", "5. Buaya", "3. Cicak", "4. Ular", "1. Tokek"];
daftarHewan.sort();
for (var i = 0; i < daftarHewan.length; i++) {
    console.log(daftarHewan[i]);
}

console.log("----------")

//soal 2
function introduce() {
    console.log("Nama saya " + data.name + ", umur saya " + data.age + " tahun, " + "alamat saya di " + data.address + " dan saya punya hobby yaitu " + data.hobby)
}
var data = { name: "jhon", age: 30, address: "Jalan Pelesiran", hobby: "Gamming" }

introduce(data)

console.log("----------")

//soal 3
function checkVokal(n) {
    return ['a', 'A', 'i', 'I', 'u', 'U', 'e', 'E', 'o', 'O'].indexOf(n) !== -1;
}

function hitung_huruf_vokal(str) {
    var strArray = str.split("");
    var jumlahVokal = 0;
    for (var i = 0; i < strArray.length; i++) {
        if (checkVokal(strArray[i]) === true) {
            jumlahVokal++;
        }

    }
    return jumlahVokal;
}
var hitung_1 = hitung_huruf_vokal("Muhammad")
var hitung_2 = hitung_huruf_vokal("Iqbal")

console.log(hitung_1, hitung_2)

console.log("----------")

//Soal 4
function hitung(n) {
    var a = (2 * n)
    var b = (a - 2)
    return b;

}

console.log(hitung(0))
console.log(hitung(1))
console.log(hitung(2))
console.log(hitung(3))
console.log(hitung(5)) 