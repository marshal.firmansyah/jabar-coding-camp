//soal1
luas = (a, b) => {
    let panjang = a;
    let lebar = b;
    return (panjang * lebar)
}

keliling = (c, d) => {
    let panjang = c;
    let lebar = d;
    return (2 * panjang + 2 * lebar)
}
console.log(luas(5, 4))
console.log(keliling(5, 4))

console.log("---------")

//soal2
const newFunction = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName: function () {
            console.log(firstName + " " + lastName)
        }
    }
}

//Driver Code 
newFunction("William", "Imoh").fullName()

console.log("---------")

//soal3
const newObject = {
    firstName: "Marshal",
    lastName: "Firmansyah",
    address: "Garut",
    hobby: "Basket Ball"
}

// Driver code
const { firstName, lastName, address, hobby } = newObject
console.log(firstName, lastName, address, hobby)

console.log("----------")

//soal4
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = west.concat(east)

//Driver Code
console.log(combined)
var combinedArray = [...west, ...east]
console.log(combinedArray)

console.log("---------")

//soal5
const planet = "earth"
const view = "glass"
var before = 'Lorem ' + view + 'dolor sit amet, ' + 'consectetur adipiscing elit,' + planet
const str = `${before}`
console.log(str)
