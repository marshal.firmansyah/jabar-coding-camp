// soal 1
var pertama = "saya sangat senang hari ini";
var kedua = "belajar javascript itu keren";

var letter = "javascript";
var upper = letter.toUpperCase();

var hasil1 = pertama.substring(0, 4);
var hasil2 = pertama.substring(12, 18);
var hasil3 = kedua.substring(0, 7);

console.log(hasil1, hasil2, hasil3, upper)
console.log('-------------')

// soal 2
var kataPertama = "10";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "6";

var strInt1 = parseInt(kataPertama);
var strInt2 = parseInt(kataKedua);
var strInt3 = parseInt(kataKetiga);
var strInt4 = parseInt(kataKeempat);

var hasil = (strInt2 * strInt3) + (strInt1 + strInt4)
console.log(hasil)
console.log('-------------')

// soal 3
var kalimat = 'wah javascript itu keren sekali';

var kataPertama = kalimat.substring(0, 3);
var kataKedua = kalimat.substring(4, 14);
var kataKetiga = kalimat.substring(15, 18);
var kataKeempat = kalimat.substring(19, 24);
var kataKelima = kalimat.substring(25, 31);

console.log('Kata Pertama: ' + kataPertama);
console.log('Kata Kedua: ' + kataKedua);
console.log('Kata Ketiga: ' + kataKetiga);
console.log('Kata Keempat: ' + kataKeempat);
console.log('Kata Kelima: ' + kataKelima);