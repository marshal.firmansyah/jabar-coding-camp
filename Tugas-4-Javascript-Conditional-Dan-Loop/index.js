//soal 1
var nilai = 80;

if (nilai >= 85) {
    console.log("Nilai Anda Adalah", "A")
} else if (nilai >= 75 && nilai < 85) {
    console.log("Nilai Anda Adalah", "B")
} else if (nilai >= 65 && nilai < 75) {
    console.log("Nilai Anda Adalah", "C")
} else if (nilai >= 55 && nilai < 65) {
    console.log("Nilai Anda Adalah", "D")
} else if (nilai < 55) {
    console.log("Nilai Anda Adalah", "E")
}

console.log("----------")

//soal 2
var tanggal = 6;
var bulan = 3;
var tahun = 2001;

switch (bulan) {
    case 1: { console.log(tanggal + ' Januari ' + tahun); break; }
    case 2: { console.log(tanggal + ' Februari ' + tahun); break; }
    case 3: { console.log(tanggal + ' Maret ' + tahun); break; }
    case 4: { console.log(tanggal + ' April ' + tahun); break; }
    case 5: { console.log(tanggal + ' Mei ' + tahun); break; }
    case 6: { console.log(tanggal + ' Juni ' + tahun); break; }
    case 7: { console.log(tanggal + ' Juli ' + tahun); break; }
    case 8: { console.log(tanggal + ' Agustus ' + tahun); break; }
    case 9: { console.log(tanggal + ' September ' + tahun); break; }
    case 10: { console.log(tanggal + ' Oktober ' + tahun); break; }
    case 11: { console.log(tanggal + ' November ' + tahun); break; }
    case 12: { console.log(tanggal + ' Desember ' + tahun); break; }
    default: { console.log('error'); }
}

console.log("----------")

//soal 3
var n = 3;
var pagar = '';
for (var i = 0; i < n; i++) {
    for (var j = 0; j <= i; j++) {
        pagar += '#';
    }
    pagar += '\n';
}
console.log(pagar);

console.log("=====")

var m = 7;
var pager = '';
for (var i = 0; i < m; i++) {
    for (var j = 0; j <= i; j++) {
        pager += '#';
    }
    pager += '\n';
}
console.log(pager);

console.log("----------")

// soal 4
var m = 10;
var soalEmpat = "";
for (var i = 1; i <= m; i += 3) {
    console.log(i + " - I love programming")
    var j = i + 1;
    var k = j + 1;
    soalEmpat += "=";
    while (j < k && j < m) {
        console.log(j + " - I love Javascript")
        var l = j + 1;
        var n = l + 1;
        soalEmpat += "=";
        while (l < n && k <= m) {
            console.log(k + " - I love VueJs")
            soalEmpat += "=";
            l += 3
        }
        j += 3;
        console.log(soalEmpat)
    }
}
